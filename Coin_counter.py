# import classifier
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import train_test_split

import math
import numpy as np
import glob
import cv2


# function to predict material, from wihich coin is made
def predict_material(region_of_intrest):
    histogram = calculate_histogram(region_of_intrest)
    s = clf.predict([histogram])
    return Material[int(s)]


# function to calculate histogram from image
def calculate_histogram(input_image):
    mask = np.zeros(input_image.shape[:2], dtype="uint8")
    (w, h) = (int(input_image.shape[1] / 2), int(input_image.shape[0] / 2))
    cv2.circle(mask, (w, h), 60, 255, -1)
    # calcHist expects a list of images, color channels, mask, bins, ranges
    h = cv2.calcHist([input_image], [0, 1, 2], mask, [8, 8, 8], [0, 256, 0, 256, 0, 256])
    return cv2.normalize(h, h).flatten()


def calculate_histogram_from_file(file):
    img = cv2.imread(file)
    return calculate_histogram(img)


# Load, scale, copy and grayscale image from file
def prepare_image(filename):
    unprepared_image = cv2.imread(filename)

    d_tmp = 1024 / unprepared_image.shape[1]
    dimension = (1024, int(unprepared_image.shape[0] * d_tmp))
    unprepared_image = cv2.resize(unprepared_image, dimension, interpolation=cv2.INTER_AREA)
    output_image = unprepared_image.copy()
    gray_image = cv2.cvtColor(unprepared_image, cv2.COLOR_BGR2GRAY)
    clahe_image = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
    gray_image = clahe_image.apply(gray_image)
    return unprepared_image, gray_image, output_image


# define Enum class to store materials
class Enum(tuple): __getattr__ = tuple.index


Material = Enum(('miedz', 'srebro', 'zloty_2', 'zloty_5'))


image, gray, output = prepare_image("test3.jpg")

sample_images = []
# locate sample image files
sample_images_grosze_j = glob.glob("monety\grosze_j/*")
sample_images_grosze_d = glob.glob("monety\grosze_d/*")
sample_images_zloty_2 = glob.glob("monety\zloty_2/*")
sample_images_zloty_5 = glob.glob("monety\zloty_5/*")

# define training data and labels
X = []
y = []

# compute and store training data and labels
for i in sample_images_grosze_j:
    X.append(calculate_histogram_from_file(i))
    y.append(Material.miedz)
for i in sample_images_grosze_d:
    X.append(calculate_histogram_from_file(i))
    y.append(Material.srebro)
for i in sample_images_zloty_2:
    X.append(calculate_histogram_from_file(i))
    y.append(Material.zloty_2)
for i in sample_images_zloty_5:
    X.append(calculate_histogram_from_file(i))
    y.append(Material.zloty_5)

clf = MLPClassifier(solver="lbfgs")

# split samples into training and test data, train and score classifier
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=.2)
clf.fit(X_train, y_train)
score = int(clf.score(X_test, y_test) * 100)

blurred = cv2.GaussianBlur(gray, (7, 7), 0)
circles = cv2.HoughCircles(blurred, cv2.HOUGH_GRADIENT, dp=2.2, minDist=100,
                           param1=200, param2=100, minRadius=50, maxRadius=120)

diameter = []
materials = []
coordinates = []

count = 0
if circles is not None:
    # append radius to list of diameters (we don't bother to multiply by 2)
    for (x, y, r) in circles[0, :]:
        diameter.append(r)

    # convert coordinates and radii to integers
    circles = np.round(circles[0, :]).astype("int")

    # loop over coordinates and radii of the circles
    for (x, y, d) in circles:
        count += 1

        # add coordinates to list
        coordinates.append((x, y))

        # extract region of interest
        roi = image[y - d:y + d, x - d:x + d]

        # try recognition of material type and add result to list
        material = predict_material(roi)
        materials.append(material)

        # draw contour and results in the output image
        cv2.circle(output, (x, y), d, (0, 255, 0), 2)
        cv2.putText(output, material,
                    (x - 40, y), cv2.FONT_HERSHEY_PLAIN,
                    1.5, (0, 255, 0), thickness=2, lineType=cv2.LINE_AA)

# get biggest diameter
biggest = max(diameter)
i = diameter.index(biggest)

# scale everything according to maximum diameter

if materials[i] == "zloty_2":
    diameter = [x / biggest * 21.50 for x in diameter]
    scaledTo = "Przeskalowano do 2 zlotych"
elif materials[i] == "zloty_5":
    diameter = [x / biggest * 24.00 for x in diameter]
    scaledTo = "Przeskalowano do 5 zlotych"
elif materials[i] == "srebro":
    diameter = [x / biggest * 23.0 for x in diameter]
    scaledTo = "Przeskalowano do 1 zloty"
elif materials[i] == "miedz":
    diameter = [x / biggest * 19.50 for x in diameter]
    scaledTo = "Przeskalowano do 5 gr"

else:
    scaledTo = "unable to scale.."

i = 0
total = 0
while i < len(diameter):
    d = diameter[i]
    coin_material = materials[i]
    (x, y) = coordinates[i]
    t = "Unknown"

    # compare to known diameters with some margin for error
    if math.isclose(d, 24.00, abs_tol=1.25) and coin_material == "zloty_5":
        t = "5 zl"
        total += 500
    elif math.isclose(d, 21.50, abs_tol=10.0) and coin_material == "zloty_2":
        t = "2 zl"
        total += 200
    elif math.isclose(d, 23.00, abs_tol=1.25) and coin_material == "srebro":
        t = "1 zl"
        total += 100
    elif math.isclose(d, 20.50, abs_tol=1.0) and coin_material == "srebro":
        t = "50 gr"
        total += 50
    elif math.isclose(d, 18.50, abs_tol=1.55) and coin_material == "srebro":
        t = "20 gr"
        total += 20
    elif math.isclose(d, 16.50, abs_tol=1.00) and coin_material == "srebro":
        t = "10 gr"
        total += 10
    elif math.isclose(d, 19.50, abs_tol=1.25) and coin_material == "miedz":
        t = "5 gr"
        total += 5
    elif math.isclose(d, 17.50, abs_tol=1.25) and coin_material == "miedz":
        t = "2 gr"
        total += 2
    elif math.isclose(d, 15.50, abs_tol=2.5) and coin_material == "miedz":
        t = "1 gr"
        total += 1

    # write result on output image
    cv2.putText(output, t,
                (x - 40, y + 22), cv2.FONT_HERSHEY_PLAIN,
                1.5, (255, 255, 255), thickness=2, lineType=cv2.LINE_AA)
    i += 1

# resize output image while retaining aspect ratio
d = 500 / output.shape[1]
dim = (500, int(output.shape[0] * d))
image = cv2.resize(image, dim, interpolation=cv2.INTER_AREA)
output = cv2.resize(output, dim, interpolation=cv2.INTER_AREA)

# write summary on output image
cv2.putText(output, scaledTo,
            (5, output.shape[0] - 40), cv2.FONT_HERSHEY_PLAIN,
            1.0, (0, 0, 255), lineType=cv2.LINE_AA)
cv2.putText(output, "Wykryto monet {}, PLN {:2}".format(count, total / 100),
            (5, output.shape[0] - 24), cv2.FONT_HERSHEY_PLAIN,
            1.0, (0, 0, 255), lineType=cv2.LINE_AA)
cv2.putText(output, "Trafnosc: {}%".format(score),
            (5, output.shape[0] - 8), cv2.FONT_HERSHEY_PLAIN,
            1.0, (0, 0, 255), lineType=cv2.LINE_AA)

# show output and wait for key to terminate program
cv2.imshow("Output", np.hstack([image, output]))
cv2.waitKey(0)
