1. Cel projektu

Celem projektu było stworzenie programu, który na podstawie wejściowego zdjęcia monet zrobionego „z lotu ptaka” rozpozna nominał każdej z monet, podpisze go na obrazie, oraz poda sumaryczną wartość pieniędzy na zdjęciu


2. Technologia

Projekt został wykonany w języku python, wersja 3. Do przetwarzania obrazów wykorzystano bibliotekę opencv, do prostego maszynowego: skrlen. Oprócz tego wykorzystano biblioteki math, glop oraz numpy



3. Zasada Działania

    1.Początkowo pobieramiy zdjęcie z pliku, skalujemy je, zwiększamy kontrast oraz tworzymy jego szarą wersję
    
    2.Nastęnie z podanych folderów pobieramy z odpowiednich folderów wzorce poszczególnych materiałów monet(miedziane,srebrne, 2zł, 5zł), tworzymy ich histogramy i przypisujemy do sieci neuronowej - MLPClassifier
    
    3.Stosujemy filtr GaussianBlur na szarej wersji zdjęcia po czym za pomocą algorytmu HoughCircles wykrywamy okręgi(monety) i zapisujemy ich współrzędne
    
    4.Iterując po kolejnych wykrytych okręgach: Na podstawie histogramu wyliczonego z obszaru monety oraz sieci neuronowej przewidujemy z jakiego materiału został wykonany pieniądz
    
    5.Wybierana jest moneta o największej średnicy,po porównaniu jej materiału i średnicy następuje skalowane średnic pozostałych monet
    
    6.Następuje kolejna pętla iteracji: na podstawie materiału monety, jej średnicy, danych o prawdziwych średnicach monet, każda z nich zostaje podpisana jakim jest nominałem
    
    7.Na koniec następuje zliczenie wszystkich wartości monet oraz wypisanie obrazu wejściowego i wyjściowego na ekran

4. Przykładowe dane wejściowe i wyniki


![alt text](1.png)

